@extends('layout.admin_layouts')

@section('title', 'Administrator | Tambah Mahasiswa')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur"
        navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark"
                            href="{{ route('dashboard') }}">Administrator</a></li>
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark"
                            href="{{ route('admin.mahasiswa') }}">Data Mahasiswa</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Tambah Data Mahasiswa</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">Tambah Data Mahasiswa</h6>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                </div>
                <ul class="navbar-nav  justify-content-end">
                    <li class="nav-item d-flex align-items-center">
                        <i class="fa fa-user"></i>
                        <span class="d-sm-inline text-sm">&nbsp;{{ Auth::user()->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Forms -->
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pb-0 p-3">
                        <div class="container">
                            <form class="row g-3" method="POST" action="{{ route('mahasiswa.store') }}"
                                enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="col-md-6">
                                    <label for="username" class="form-label">Username</label>
                                    <input type="text" class="form-control @error('username') is-invalid @enderror"
                                        id="username" name="username" placeholder="Username"
                                        value="{{ old('username') }}">
                                    @error('username')
                                        <div class="invalid-feedback">
                                            Username tidak boleh kosong!
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="nama" class="form-label">Nama Mahasiswa</label>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                                        name="name" placeholder="Nama Mahasiswa" value="{{ old('name') }}">
                                    @error('name')
                                        <div class="invalid-feedback">
                                            Nama Mahasiswa tidak boleh kosong!
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="nim" class="form-label">NIM</label>
                                    <input type="text" class="form-control @error('nim') is-invalid @enderror" id="nim"
                                        name="nim" placeholder="NIM Mahasiswa" value="{{ old('nim') }}">
                                    @error('nim')
                                        <div class="invalid-feedback">
                                            NIM tidak boleh kosong!
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="kampus" class="form-label">Nama Kampus</label>
                                    <input type="text" class="form-control @error('kampus') is-invalid @enderror"
                                        id="kampus" name="kampus" placeholder="Nama Kampus" value="{{ old('kampus') }}">
                                    @error('kampus')
                                        <div class="invalid-feedback">
                                            Nama Kampus tidak boleh kosong!
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="nama" class="form-label">Nomor Telepon</label>
                                    <input type="number" class="form-control @error('no_tlp') is-invalid @enderror"
                                        id="no_tlp" name="no_tlp" placeholder="Nomor Telephone"
                                        value="{{ old('no_tlp') }}">
                                    @error('no_tlp')
                                        <div class="invalid-feedback">
                                            Nomor Telepon tidak boleh kosong!
                                        </div>
                                    @enderror
                                </div>
                                {{-- <div class="col-md-6">
                                    <label for="nama" class="form-label">Email</label>
                                    <input type="text" class="form-control @error('email') is-invalid @enderror" id="email"
                                        name="email" placeholder="Email" value="{{ old('email') }}">
                                    @error('email')
                                        <div class="invalid-feedback">
                                            Email tidak boleh kosong!
                                        </div>
                                    @enderror
                                </div> --}}
                                <div class="col-md-6">
                                    <label for="nama" class="form-label">Password</label>
                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                        id="password" name="password" placeholder="Password"
                                        value="{{ old('password') }}">
                                    @error('password')
                                        <div class="invalid-feedback">
                                            Password tidak boleh kosong!
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="harga" class="form-label">Kode Referal</label>
                                    <input type="text" class="form-control" id="kode_referal" name="kode_referal"
                                        placeholder="Kode Referal" value="{{ Str::random(15) }}">
                                </div>
                                <div class="col-12">
                                    <a href="{{ route('admin.mahasiswa') }}" type="button"
                                        class="btn btn-secondary float-start">Batal</a>
                                    <button type="submit" class="btn btn-primary float-end">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
