@extends('layout.app')

@section('title', 'Contact Us')

@section('content')
    <!-- Navbar -->
    <header class="main-header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu"
                        aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand" href="{{ route('welcome') }}"><img
                            src="{{ asset('images/nyawiji-modified.png') }}" class="logo" alt=""></a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                        <li class="nav-item"><a class="nav-link" href="{{ route('welcome') }}">Home</a></li>
                        {{-- <li class="nav-item"><a class="nav-link" href="#product">Product</a></li> --}}
                        <li class="nav-item"><a class="nav-link" href="{{ route('about') }}">About Us</a></li>
                        <!-- <li class="dropdown megamenu-fw">
                                <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">Product</a>
                                <ul class="dropdown-menu megamenu-content" role="menu">
                                    <li>
                                        <div class="row">
                                            <div class="col-menu col-md-3">
                                                <h6 class="title">Top</h6>
                                                <div class="content">
                                                    <ul class="menu-col">
                                                        <li><a href="shop.html">Jackets</a></li>
                                                        <li><a href="shop.html">Shirts</a></li>
                                                        <li><a href="shop.html">Sweaters & Cardigans</a></li>
                                                        <li><a href="shop.html">T-shirts</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-menu col-md-3">
                                                <h6 class="title">Bottom</h6>
                                                <div class="content">
                                                    <ul class="menu-col">
                                                        <li><a href="shop.html">Swimwear</a></li>
                                                        <li><a href="shop.html">Skirts</a></li>
                                                        <li><a href="shop.html">Jeans</a></li>
                                                        <li><a href="shop.html">Trousers</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-menu col-md-3">
                                                <h6 class="title">Clothing</h6>
                                                <div class="content">
                                                    <ul class="menu-col">
                                                        <li><a href="shop.html">Top Wear</a></li>
                                                        <li><a href="shop.html">Party wear</a></li>
                                                        <li><a href="shop.html">Bottom Wear</a></li>
                                                        <li><a href="shop.html">Indian Wear</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-menu col-md-3">
                                                <h6 class="title">Accessories</h6>
                                                <div class="content">
                                                    <ul class="menu-col">
                                                        <li><a href="shop.html">Bags</a></li>
                                                        <li><a href="shop.html">Sunglasses</a></li>
                                                        <li><a href="shop.html">Fragrances</a></li>
                                                        <li><a href="shop.html">Wallets</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li> -->
                        {{-- <li class="nav-item"><a class="nav-link" href="service.html">Our Service</a></li> --}}
                        <li class="nav-item active"><a class="nav-link" href="{{ route('contact') }}">Contact Us</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('listkeranjangbelanja') }}"><i
                                    class="fas fa-cart-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <!-- End Navbar -->

    <!-- Start Contact Us  -->
    <div class="contact-box-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Contact Info</h1>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">
                            <div class="contact-info-left">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut
                                    ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc
                                    tristique purus turpis. Maecenas vulputate. </p>
                                <ul>
                                    <li>
                                        <p><i class="fas fa-map-marker-alt"></i>{!! $profile->alamat !!}</p>
                                    </li>
                                    <li>
                                        <p><i class="fas fa-phone-square"></i><a
                                                href="https://wa.me/">{{ $profile->no_tlp }}</a></p>
                                    </li>
                                    <li>
                                        <p><i class="fas fa-envelope"></i>{{ $profile->email }}</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Contact -->

    @include('layout.component.footer')
@endsection
