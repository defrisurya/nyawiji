@extends('layout.app')

@section('title', 'Info Produk')

@section('content')
    @include('front.home.replika.layouts.component.navbar')
    <!-- Start Shop Detail  -->
    <div class="shop-detail-box-main">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-6">
                    <div id="carousel-example-1" class="single-product-slider carousel slide detail" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            @if (isset($produk->foto1))
                                <div class="carousel-item active">
                                    <div class="gallery">
                                        <a href="{{ asset('uploads/produk/' . $produk->foto1) }}"><img
                                                src="{{ asset('uploads/produk/' . $produk->foto1) }}" /></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            @endif
                            @if (isset($produk->foto2))
                                <div class="carousel-item">
                                    <div class="gallery">
                                        <a href="{{ asset('uploads/produk/' . $produk->foto2) }}"><img
                                                src="{{ asset('uploads/produk/' . $produk->foto2) }}" /></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            @endif
                            @if (isset($produk->foto3))
                                <div class="carousel-item">
                                    <div class="gallery">
                                        <a href="{{ asset('uploads/produk/' . $produk->foto3) }}"><img
                                                src="{{ asset('uploads/produk/' . $produk->foto3) }}" /></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            @endif
                            @if (isset($produk->foto4))
                                <div class="carousel-item">
                                    <div class="gallery">
                                        <a href="{{ asset('uploads/produk/' . $produk->foto4) }}"><img
                                                src="{{ asset('uploads/produk/' . $produk->foto4) }}" /></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            @endif
                        </div>
                        <a class="carousel-control-prev" href="#carousel-example-1" role="button" data-slide="prev">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-example-1" role="button" data-slide="next">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <span class="sr-only">Next</span>
                        </a>
                        {{-- <ol class="carousel-indicators">
                            <li data-target="#carousel-example-1" data-slide-to="0" class="active">
                                <img class="d-block w-100 img-fluid" src="{{ asset('uploads/produk/'. $produk->foto1) }}" alt="" />
                            </li>
                            <li data-target="#carousel-example-1" data-slide-to="1">
                                <img class="d-block w-100 img-fluid" src="{{ asset('uploads/produk/'. $produk->foto2) }}" alt="" />
                            </li>
                            <li data-target="#carousel-example-1" data-slide-to="2">
                                <img class="d-block w-100 img-fluid" src="{{ asset('uploads/produk/'. $produk->foto3) }}" alt="" />
                            </li>
                            <li data-target="#carousel-example-1" data-slide-to="3">
                                <img class="d-block w-100 img-fluid" src="{{ asset('uploads/produk/'. $produk->foto4) }}" alt="" />
                            </li>
                        </ol> --}}
                    </div>
                </div>
                <div class="col-xl-7 col-lg-7 col-md-6">
                    <div class="single-product-details">
                        <h2>{{ $produk->nama }}</h2>
                        <h5>Rp. {{ number_format($produk->harga) }}</h5>
                        <p>
                        <h4>Product Description</h4>
                        {!! $produk->deskripsi !!}
                        <form action="{{ url('keranjangbeli/Shop', $user->kode_referal) }}" method="POST">
                            @csrf
                            <input type="hidden" name="nama" value="{{ $produk->nama }}">
                            <input type="hidden" name="produkid" value="{{ $produk->id }}">
                            <input type="hidden" name="userid" value="{{ $user->kode_referal }}">
                            <ul>
                                <li>
                                    <div class="form-group size-st">
                                        <label class="size-label">Size Ready</label>
                                        <select id="size" name="ukuran" class="selectpicker show-tick form-control">
                                            <option value="">-- Pilih Ukuran --</option>
                                            @foreach ($produk->size as $item)
                                                <option value="{{ $item->ukuran }}">{{ $item->ukuran }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group quantity-box">
                                        <label class="control-label">Color Ready</label>
                                        <select id="color" name="warna" class="selectpicker show-tick form-control">
                                            <option value="">-- Pilih Warna --</option>
                                            @foreach ($produk->warna as $item)
                                                <option value="{{ $item->warna }}">{{ $item->warna }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group quantity-box">
                                        <label class="control-label">Quantity</label>
                                        <input type="text" name="qty">
                                    </div>
                                </li>
                            </ul>
                            <div class="add-to-btn">
                                <div class="add-comp">
                                    {{-- <a class="btn hvr-hover link" target="__blank" href="https://wa.me/0888705770?text=Nama%20Produk%20%3A%20{{$produk->nama}}%0AUkuran%20%3A%20%0AWarna%20%3A%20">
                                            <i class="fab fa-whatsapp"></i>
                                            &nbsp;Whatsapp Now
                                        </a> --}}
                                    {{-- <button class="btn hvr-hover link" type="submit"><i
                                            class="fab fa-whatsapp"></i>&nbsp;Whatsapp Now</button> --}}
                                    <button class="btn hvr-hover link"><i class="fas fa-cart-plus"></i>&nbsp;Masukkan
                                        Keranjang</button>
                                </div>
                            </div>
                        </form>
                        </p>
                    </div>
                </div>
            </div>

            <div class="row my-5" id="product">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Other Products</h1>
                    </div>
                    <div class="row special-list">
                        @forelse ($data as $item)
                            <div class="col-lg-3 col-md-6 special-grid">
                                <div class="products-single fix">
                                    <div class="box-img-hover">
                                        <div class="type-lb"></div>
                                        <img src="{{ asset('uploads/produk/' . $item->foto1) }}" class="img-fluid">
                                        <a class="cart"
                                            href="{{ url('infoproduct/' . $item->id . '/Shop', $user->kode_referal) }}">
                                            <div class="mask-icon">
                                                <p>Info Product</p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="why-text">
                                        <h4>{{ $item->nama }}</h4>
                                        <h5>Rp. {{ number_format($item->harga) }}</h5>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <i>Data tidak tersedia</i>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Cart -->

    <!-- Footer -->
    @include('layout.component.footer')
@endsection

@section('script')
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> --}}
    <script>
        (function() {
            var $gallery = new SimpleLightbox('.gallery a', {});
        })();
    </script>
@endsection
