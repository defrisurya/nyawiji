@extends('layout.admin_layouts')

@section('title', 'Administrator | Data Transaksi')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur"
        navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark"
                            href="{{ route('dashboard') }}">Administrator</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Data Transaksi</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">Data Transaksi</h6>
                <li class="nav-item d-xl-none mt-3 d-flex align-items-center">
                    <a href="#" class="nav-link text-body p-0" id="iconNavbarSidenav">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </a>
                </li>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                    <div class="input-group">
                    </div>
                </div>
                <ul class="navbar-nav  justify-content-end">
                    <li class="nav-item d-flex align-items-center">
                        <i class="fa fa-user"></i>
                        <span class="d-sm-inline text-sm">&nbsp;{{ Auth::user()->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                                <h6 class="mb-0">Data Transaksi</h6>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pb-0 p-3">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th
                                            class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">
                                            No
                                        </th>
                                        <th
                                            class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">
                                            Tanggal Transaksi
                                        </th>
                                        <th
                                            class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">
                                            Kode Invoice
                                        </th>
                                        <th
                                            class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">
                                            Share
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        // dd($transmhs);
                                        $no = 1;
                                    @endphp
                                    @forelse ($transmhs as $item => $value)
                                        <tr>
                                            <td>
                                                <p class="text-xs font-weight-bold mb-0">{{ $no++ }}</p>
                                            </td>
                                            <td>
                                                <p class="text-xs font-weight-bold mb-0">
                                                    {{ Carbon\Carbon::parse($value->tgl_transaksi)->isoFormat('D MMMM Y') }}
                                                </p>
                                            </td>
                                            <td>
                                                <p class="text-xs font-weight-bold mb-0">{{ $value->kode_invoice }}</p>
                                            </td>
                                            <td>
                                                <p class="text-xs font-weight-bold mb-0">Rp.
                                                    {{ number_format(($value->subtotal / 100) * 30) }}
                                                </p>
                                            </td>
                                            {{-- <td>
                                                <a href="{{ route('transaksibeli.show', $item->id) }}"
                                                    class="badge badge-sm bg-gradient-primary" data-toggle="tooltip"
                                                    data-original-title="Detail Invoice">
                                                    <i class="fa fa-file-invoice-dollar"></i>&nbsp;Detail Invoice
                                                </a>
                                            </td> --}}
                                        @empty
                                            <td class="text-sm" colspan="15" align="center">
                                                <i>Data tidak tersedia</i>
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            {{ $transmhs->links() }}
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-8">
                        </div>
                        <div class="col-4">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th>Saldo</th>
                                        <td class="font-weight-bolder mb-0">Rp.
                                            {{ number_format(($saldo / 100) * 30 - $tarik) }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
