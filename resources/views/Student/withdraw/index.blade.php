@extends('layout.admin_layouts')

@section('title', 'Administrator | Withdraw')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur"
        navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark"
                            href="{{ route('dashboard') }}">Administrator</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Withdraw</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">Whithdraw</h6>
                <li class="nav-item d-xl-none mt-3 d-flex align-items-center">
                    <a href="#" class="nav-link text-body p-0" id="iconNavbarSidenav">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </a>
                </li>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                    <div class="input-group">
                    </div>
                </div>
                <ul class="navbar-nav  justify-content-end">
                    <li class="nav-item d-flex align-items-center">
                        <i class="fa fa-user"></i>
                        <span class="d-sm-inline text-sm">&nbsp;{{ Auth::user()->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid">
        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-body p-3">
                    <div class="row">
                        <div class="col-8">
                            <div class="numbers">
                                <p class="mb-0 text-capitalize" style="color: darkslategrey">Saldo Anda</p>
                                <p class="font-weight-bolder mb-0" style="color: darkslategrey">
                                    Rp. {{ number_format(($saldo / 100) * 30 - $tarik) }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                                <h6 class="mb-0">Data Withdraw</h6>
                            </div>
                            <div class="col-6 text-end">
                                <a class="btn bg-gradient-success text-dark mb-0" style="font-size: 12px"
                                    href="{{ route('withdraw.create') }}"><i
                                        class="fas fa-plus"></i>&nbsp;&nbsp;Withdraw</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pb-0 p-3">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th
                                            class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">
                                            No
                                        </th>
                                        <th
                                            class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">
                                            Tanggal Whithdraw
                                        </th>
                                        <th
                                            class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">
                                            Kode Withdraw
                                        </th>
                                        <th
                                            class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">
                                            Nomor Rekening
                                        </th>
                                        <th
                                            class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">
                                            Jumlah Withdraw
                                        </th>
                                        <th
                                            class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">
                                            Status
                                        </th>
                                        <th
                                            class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">
                                        </th>
                                        <th class="text-secondary opacity-7"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        // dd($transmhs);
                                        $no = 1;
                                    @endphp
                                    @forelse ($withdraw as $item)
                                        <tr>
                                            <td>
                                                <p class="text-xs font-weight-bold mb-0">{{ $no++ }}</p>
                                            </td>
                                            <td>
                                                <p class="text-xs font-weight-bold mb-0">
                                                    {{ Carbon\Carbon::parse($item->creted_at)->isoFormat('D MMMM Y') }}
                                                </p>
                                            </td>
                                            <td>
                                                <p class="text-xs font-weight-bold mb-0">{{ $item->kode_withdraw }}</p>
                                            </td>
                                            <td>
                                                <p class="text-xs font-weight-bold mb-0">{{ $item->no_rek }}</p>
                                            </td>
                                            <td>
                                                <p class="text-xs font-weight-bold mb-0">Rp.
                                                    {{ number_format($item->jumlah) }}</p>
                                            </td>
                                            <td>
                                                @if ($item->status == 'Proses')
                                                    <p class="text-xs font-weight-bold mb-0" style="color: orange">
                                                        {{ $item->status }}
                                                    </p>
                                                @endif
                                                @if ($item->status == 'Success')
                                                    <p class="text-xs font-weight-bold mb-0" style="color: rgb(6, 226, 6)">
                                                        {{ $item->status }}
                                                    </p>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="https://wa.me/{{ $profile->no_tlp }}?text=Permintaan%20withdraw%20untuk%20kode%20Withdraw%20{{ $item->kode_withdraw }}%20dengan%20jumlah%20penarikan%20Rp.%20{{ number_format($item->jumlah) }}"
                                                    target="_blank" class="btn btn-success"><i
                                                        class="fab fa-whatsapp"></i>&nbsp;WhatsApp</a>
                                            </td>
                                            {{-- <td>
                                                <a href="{{ route('transaksibeli.show', $item->id) }}"
                                                    class="badge badge-sm bg-gradient-primary" data-toggle="tooltip"
                                                    data-original-title="Detail Invoice">
                                                    <i class="fa fa-file-invoice-dollar"></i>&nbsp;Detail Invoice
                                                </a>
                                            </td> --}}
                                        @empty
                                            <td class="text-sm" colspan="15" align="center">
                                                <i>Data tidak tersedia</i>
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            {{ $withdraw->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.0/dist/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
@endsection
