<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';

    protected $fillable = [
        'namaprofile',
        'foto1',
        'foto2',
        'foto3',
        'deskripsi',
        'alamat',
        'email',
        'no_tlp'
    ];
}
