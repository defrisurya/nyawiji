<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksibeli extends Model
{
    protected $table = 'transaksis';
    protected $fillable = [
        'user_id',
        'kode_invoice',
        'tgl_transaksi',
        'alamat_asal',
        'namacust',
        'notlpcust',
        'alamatcust',
        'kurir',
        'ongkir',
        'total_harga',
        'total_qty',
        'total_berat',
        'provinsi',
        'kabupaten',
        'prov_id',
        'kab_id',
        'status_pembayaran',
    ];

    public function detailtransaksi()
    {
        return $this->hasMany(Detailtransaksibeli::class, 'transaksi_id', 'id');
    }
}
