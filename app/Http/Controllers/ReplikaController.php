<?php

namespace App\Http\Controllers;

use App\Product;
use App\Profile;
use App\User;
use App\Warna;
use App\Size;
use App\Keranjangbeli;
use App\Transaksibeli;
use App\Detailtransaksibeli;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ReplikaController extends Controller
{
    public function index($user){
        $user = User::where('kode_referal', $user)->first();
        // dd($user->name);
        $profile = Profile::first();
        $product = Product::all();
        // $cookie = cookie('dw-afiliasi', json_encode($user), 2880);
        // dd($cookie);

        return view('front.home.replika.welcome', compact('product', 'user', 'profile'/* , 'cat' */));
    }

    public function detailProduk($id, $user) {
        $user = User::where('kode_referal', $user)->first();
        $profile = Profile::first();
        $produk = Product::with('size', 'warna')->find($id);
        $data = Product::all();
        // return $user;
        // dd($user);
        return view('front.home.replika.detail', compact('produk', 'profile', 'data', 'user'));
    }

    public function keranjangBeli(Request $request, $user){
        $user = User::where('kode_referal', $user)->first();
        // dd($cek);
        // $carts = json_decode($request->cookie('rp-carts'), true);
        $produk = Product::find($request->produkid) and Size::find($request->sizeid) and Warna::find($request->warnaid);
            // dd($produk);
            $produk[$request->produkid] = [
                'qty' => $request->qty,
                'produk_id' => $produk->id,
                'foto1' => $produk->foto1,
                'nama' => $produk->nama,
                'berat' => $produk->berat,
                'harga' => $produk->harga,
                'ukuran' => $request->ukuran,
                'warna' => $request->warna,
                'status' => 'Keranjang',
            ];
            // dd($produk[$request->produkid]);
            Keranjangbeli::create($produk[$request->produkid]);

        // $cookie = cookie('rp-carts', json_encode($carts), 2880);
            // dd($cookie);

        return redirect(url('listkeranjangbelanja/Shop', $user->kode_referal))/* ->cookie($cookie) */;
    }

    public function keranjangBelanja(Request $request, $user){
        // $carts = session("carts");
        $carts = Keranjangbeli::where($request->produkid)->where('status', 'Keranjang')->get();
        // dd($carts);
        if ($carts == null) {
            toast('Keranjang Pembelian Masih Kosong', 'info');
            return redirect()->route('welcome');
        }
        // dd($carts);
        // foreach ($carts as $key => $item) {
            //     $produk = Product::find($item['produk_id']);
            //     if ($produk['stok'] <= 0) {
                //         unset($carts[$key]);
                //         $cookie = cookie('rp-carts', json_encode($carts), 2880);
                //         alert()->info('Produk Yang Dipilih Sudah Tidak Tersedia', 'Pilih Produk Lainnya');
                //         return redirect()->route('listkeranjangbelanja')->cookie($cookie);
                //     }
                // }
                $produk = Product::where('id', $request->produkid)->get();
                $warna = Warna::where('id', $request->produkid)->get();
                $ukuran = Size::where('id', $request->produkid)->get();
                $user = User::where('kode_referal', $user)->first();
        // foreach ($carts as $key => $item) {
        // }

        $provinsi = app('App\Http\Controllers\RajaongkirController')->get_province();
        // dd($provinsi);
        // $carts = json_decode(request()->cookie('rp-carts'), true);
        // dd($carts);
        // if ($carts == null) {
        //     toast('Keranjang Masih Kosong', 'info');
        //     return redirect()->route('welcome');
        // }

        // $id = auth()->user()->id;
        // $userdetail = UserDetail::where('user_id', $id)->first();

        // dd($userdetail);
        // dd($carts);

        // hitung berat
        $totalberat = 0;
        foreach ($carts as $r) {
            $totalberat += $r['berat'];
        }
        $berat = $totalberat * 1000;

        // hitung total price
        $totalprice = 0;
        foreach ($carts as $r) {
            $totalprice += $r['harga'] * $r['qty'];
        }
        $jmlqty = 0;
        foreach ($carts as $r) {
            $jmlqty += $r['qty'];
        }
        // dd($jmlqty);
        // $jml = count($carts);
        // dd(Str::random(15));
        return view('front.home.replika.cart', compact('carts', /* 'userdetail', */ 'provinsi', 'warna', 'ukuran', 'berat', 'totalprice', 'jmlqty', 'user'));
    }

    public function prosescheckout(Request $request)
    {
        $request->validate([
            'namacust' => 'required',
            'notlpcust' => 'required',
            'alamatcust' => 'required',
        ]);

        // proses simpan transaksi menggunakan cookie
        // dd($request->all());
        $data = User::where('kode_referal', $request->kode_referal)->first();
        // dd($data->id);

        $transaksi = new Transaksibeli;
        $transaksi->user_id = $data->id;
        $transaksi->kode_invoice = Str::random(15);
        $transaksi->tgl_transaksi = date('Y-m-d');
        $transaksi->alamat_asal = 'Bantul';
        $transaksi->kode_referal = $data->kode_referal;
        $transaksi->namacust = $request->namacust;
        $transaksi->notlpcust = $request->notlpcust;
        $transaksi->alamatcust = $request->alamatcust;
        $transaksi->kurir = $request->kurir;
        $transaksi->ongkir = $request->ongkoskirim;
        $transaksi->total_harga = $request->totalharga;
        $transaksi->total_qty = $request->totalqty;
        $transaksi->subtotal = $request->subtotal;
        $transaksi->total_berat = $request->totalberat;
        $transaksi->provinsi = $request->provinsi;
        $transaksi->kabupaten = $request->namakota;
        $transaksi->prov_id = $request->province_id;
        $transaksi->kab_id = $request->kota_id;
        $transaksi->status_pembayaran = 'Menunggu Pembayaran';
        // dd($transaksi);
        $transaksi->save();
        //simpan detail transaksi
        $carts = Keranjangbeli::where($request->produkid)->where('status', 'Keranjang')->get();
        // $carts = session("carts");
        // dd($carts);
        foreach ($carts as $key => $item) {
            $detailtransaksi = new Detailtransaksibeli;
            $detailtransaksi->transaksi_id = $transaksi->id;
            $detailtransaksi->product_id = $item['produk_id'];
            $detailtransaksi->harga = $item['harga'];
            $detailtransaksi->ukuran = $item['ukuran'];
            $detailtransaksi->warna = $item['warna'];
            $detailtransaksi->foto = $item['foto1'];
            $detailtransaksi->qty = $item['qty'];
            $detailtransaksi->save();
        }

        //Kosongkan Cart
        $carts = Keranjangbeli::where($request->produkid)->where('status', 'Keranjang')->get();
            $carts = [
                'status' => 'Checkout',
            ];
            // dd(Keranjangbeli::where($request->produkid)->where('status', 'Keranjang')->update($carts));
            Keranjangbeli::where($request->produkid)->where('status', 'Keranjang')->update($carts);

        // $carts = [];
        //KOSONGKAN DATA KERANJANG DI COOKIE
        // $cookie = cookie('rp-carts', json_encode($carts), 2880);
        // session()->forget('carts');
        //REDIRECT KE HALAMAN FINISH TRANSAKSI
        alert()->success('Transaksi Sukses','Silahkan Melakukan Konfirmasi Pembayaran');
        return redirect()->route('buktiTransaksi', $transaksi->id)/* ->cookie($cookie) */;
    }

    public function hapusproduk($key, $user)
    {
        $user = User::where('kode_referal', $user)->first();
        Keranjangbeli::where('id', $key)->delete();
        return redirect()->back();
    }
}
