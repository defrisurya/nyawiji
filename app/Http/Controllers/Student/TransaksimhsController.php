<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Transaksibeli;
use App\Withdraw;
use Illuminate\Http\Request;

class TransaksimhsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transmhs = Transaksibeli::where('user_id', auth()->user()->id)->where('status_pembayaran', 'Terbayar')->paginate(10);
        // dd($detailtrans);

        $saldo = Transaksibeli::where('user_id', auth()->user()->id)->where('status_pembayaran', 'Terbayar')->sum('subtotal');

        $tarik = Withdraw::where('user_id', auth()->user()->id)->where('status', 'Success')->sum('jumlah');

        return view('Student.datatransaksi.index', compact('transmhs', 'saldo', 'tarik'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
