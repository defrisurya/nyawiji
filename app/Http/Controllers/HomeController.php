<?php

namespace App\Http\Controllers;

use App\Kategori;
use App\Keranjangbeli;
use App\Product;
use App\Profile;
use App\Size;
use App\Transaksibeli;
use App\User;
use App\Warna;
use App\Withdraw;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $product = Product::all();
        // dd($product);
        $profile = Profile::first();
        // $cat = Kategori::all();
        // dd($profile);

        return view('front.welcome', compact('product', 'profile'/* , 'cat' */));
    }

    public function profile()
    {
        $profile = Profile::first();
        return view('front.home.profile', compact('profile'));
    }

    public function about()
    {
        $profile = Profile::first();
        return view('front.home.about', compact('profile'));
    }

    public function contact()
    {
        $profile = Profile::first();
        return view('front.home.contact', compact('profile'));
    }

    public function admin()
    {
        $total = Product::all()->count();
        $totaltransaksi = Transaksibeli::all()->count();
        $saldo = Transaksibeli::where('user_id', auth()->user()->id)->where('status_pembayaran', 'Terbayar')->sum('subtotal');
        $tarik = Withdraw::where('user_id', auth()->user()->id)->where('status', 'Success')->sum('jumlah');
        // $totalkat = Kategori::all()->count();
        // dd($total);
        $profile = Profile::first();
        return view('admin.dashboard', compact('total', 'profile',/* , 'totalkat' */'totaltransaksi', 'saldo', 'tarik'));
    }

    public function invoice($id)
    {
        $data = Transaksibeli::with(['detailtransaksi'])->find($id);
        // dd($data);
        $profile = Profile::first();
        // dd($total);
        // $profile = Profile::first();
        return view('front.home.invoice', compact('data', 'profile'));
    }

    public function print_invoice($id)
    {
        $data = Transaksibeli::with(['detailtransaksi'])->find($id);
        // dd($data);
        $profile = Profile::first();
        // $total = Product::all()->count();
        // $totalkat = Kategori::all()->count();
        // dd($total);
        // $profile = Profile::first();
        return view('front.home.invoice-print', compact('data', 'profile'/* , 'totalkat' */));
    }

    public function keranjangbeli(Request $request)
    {
        // $carts = json_decode($request->cookie('rp-carts'), true);
        $produk = Product::find($request->produkid) and Size::find($request->sizeid) and Warna::find($request->warnaid);
            // dd($produk);
            $produk[$request->produkid] = [
                'qty' => $request->qty,
                'produk_id' => $produk->id,
                'foto1' => $produk->foto1,
                'nama' => $produk->nama,
                'berat' => $produk->berat,
                'harga' => $produk->harga,
                'ukuran' => $request->ukuran,
                'warna' => $request->warna,
                'status' => 'Keranjang',
            ];
            // dd($produk[$request->produkid]);
            Keranjangbeli::create($produk[$request->produkid]);

        // $cookie = cookie('rp-carts', json_encode($carts), 2880);
            // dd($cookie);

        return redirect()->route('listkeranjangbelanja')/* ->cookie($cookie) */;
    }

    // public function kategori($id)
    // {
    //     $profile = Profile::first();
    //     $product = Product::all();
    //     $kategori = Kategori::find($id);
    //     return view('front.home.kategori', compact('profile', 'product', 'kategori'));
    // }

}
