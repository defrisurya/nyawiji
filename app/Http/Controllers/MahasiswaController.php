<?php

namespace App\Http\Controllers;

use App\Mahasiswa;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $mhs = User::where('role','mahasiswa')->paginate(5);
        $profile = Profile::first();
        // dd($produk);

        if ($request->cari) {
            if ($request->cari == "name") {
                $mhs = User::where('name')->latest()->paginate(5);
            } else {
                $mhs = User::where(function ($mhs) use ($request) {
                    $mhs->where('kode_referal', 'like', "%$request->cari%");
                })->latest()->paginate(5);
            }
        }

        return view('admin.mahasiswa.index', compact('mhs', 'profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profile = Profile::first();
        // $kategori = Kategori::all();
        return view('admin.mahasiswa.create', compact('profile'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'              => 'required',
            'username'          => 'required',
            'nim'               => 'required',
            'kampus'            => 'required',
            'no_tlp'            => 'required',
            'kode_referal'      => 'required',
        ]);

        $data = $request->all();
        // dd($data);
        User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'nim' => $data['nim'],
            'kampus' => $data['kampus'],
            'no_tlp' => $data['no_tlp'],
            'kode_referal' => $data['kode_referal'],
            'role' => 'mahasiswa',
            'password' => bcrypt($data['password']),
        ]);

        toast('Data Mahasiswa Berhasil Ditambahkan','success');
        return redirect('mahasiswa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function show(Mahasiswa $mahasiswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mhs = User::findorfail($id);
        // dd($produk->size);
        // $kategori = Kategori::all();
        $profile = Profile::first();
        return view('admin.mahasiswa.edit', compact('profile', 'mhs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'              => 'required',
            'username'          => 'required',
            'nim'               => 'required',
            'kampus'            => 'required',
            'no_tlp'            => 'required',
            'kode_referal'      => 'required',
        ]);

        $data = $request->all();
        $mhs = User::findorfail($id);
        $mhs->update([
            'name' => $data['name'],
            'username' => $data['username'],
            'nim' => $data['nim'],
            'kampus' => $data['kampus'],
            'no_tlp' => $data['no_tlp'],
            'kode_referal' => $data['kode_referal'],
            'role' => 'mahasiswa',
            'password' => bcrypt($data['password']),
        ]);

        toast('Data Mahasiswa Berhasil Diupdate','success');
        return redirect('mahasiswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('users')->where('id', $id)->delete();

        toast('Data Mahasiswa Berhasil Dihapus','success');
        return redirect()->back();
    }
}
