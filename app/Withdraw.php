<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    protected $table = 'withdraws';
    protected $fillable = ['user_id', 'kode_withdraw', 'bank_name', 'no_rek', 'nama_rek', 'jumlah','no_tlp', 'status'];
}
