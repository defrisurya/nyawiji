<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kodereferal extends Model
{
    protected $table = 'kode_referals';
    protected $fillable = [
        'mahasiswa_id',
        'kode_referal'
    ];

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class);
    }
}
