<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Gate::define('isadmin', function($user) {
            return $user->role == 'admin';
        });

        Gate::define('isStudent', function($user) {
            return $user->role == 'mahasiswa';
        });
    }
}
