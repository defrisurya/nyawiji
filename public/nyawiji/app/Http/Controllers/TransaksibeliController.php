<?php

namespace App\Http\Controllers;

use App\Detailtransaksibeli;
use App\Transaksibeli;
use App\Profile;
use Illuminate\Http\Request;

class TransaksibeliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $transaksi = Transaksibeli::with('detailtransaksi')->paginate(5);
        $profile = Profile::first();
        // dd($produk);

        if ($request->cari) {
            if ($request->cari == "kode_invoice") {
                $transaksi = Transaksibeli::where('kode_invoice')->latest()->paginate(5);
            } else {
                $transaksi = Transaksibeli::where(function ($transaksi) use ($request) {
                    $transaksi->where('tgl_transaksi', 'like', "%$request->cari%");
                })->latest()->paginate(5);
            }
        }

        return view('admin.transaksi.index', compact('transaksi', 'profile'));
    }

    public function konfirmasiBeli($id)
    {
        $data = Transaksibeli::with(['detailtransaksi'])->find($id);

        $data->update([
            'status_pembayaran' => 'Terbayar',
        ]);
        // foreach ($data['detailtransaksi'] as $p) {
        //     $produk = Transaksibeli::find($p->id);
        //     $produk->update([
        //         'status_pembayaran' => 'Terbayar',
        //     ]);
        // }
        // dd($produk);

        toast('Pembayaran Berhasil Diverifikasi', 'success');
        return redirect('transaksibeli');
    }

    public function batalBeli($id)
    {
        $data = Transaksibeli::with(['detailtransaksi'])->find($id);

        $data->update([
            'status_pembayaran' => 'Gagal',
        ]);
        // foreach ($data['detailtransaksi'] as $p) {
        //     $produk = Transaksibeli::find($p->id);
        //     $produk->update([
        //         'status_pembayaran' => 'Gagal',
        //     ]);
        // }
        // dd($produk);

        toast('Pembayaran Berhasil Diverifikasi', 'success');
        return redirect('transaksibeli');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaksibeli  $transaksibeli
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Transaksibeli::with(['detailtransaksi'])->get()->find($id);
        $profile = Profile::first();
        $transaksi = Transaksibeli::with(['detailtransaksi'])->find($id);
        // dd($transaksi);
        // $data = Product::all();
        return view('admin.transaksi.invoice-admin', compact('transaksi', 'profile', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaksibeli  $transaksibeli
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaksibeli $transaksibeli)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaksibeli  $transaksibeli
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaksibeli $transaksibeli)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaksibeli  $transaksibeli
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaksibeli $transaksibeli)
    {
        //
    }
}
