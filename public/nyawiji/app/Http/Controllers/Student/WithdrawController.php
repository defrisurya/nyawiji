<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Withdraw;
use App\Transaksibeli;
use Illuminate\Http\Request;

class WithdrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $withdraw = Withdraw::paginate(5);
        $saldo = Transaksibeli::where('user_id', auth()->user()->id)->where('status_pembayaran', 'Terbayar')->sum('subtotal');
        $tarik = Withdraw::where('user_id', auth()->user()->id)->where('status', 'Success')->sum('jumlah');
        return view('student.withdraw.index', compact('withdraw', 'saldo', 'tarik'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('student.withdraw.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_withdraw' => 'required',
            'bank_name' => 'required',
            'no_rek' => 'required',
            'nama_rek' => 'required',
            'jumlah' => 'required',
        ]);

        $data = new Withdraw;
        $data->user_id = auth()->user()->id;
        $data->kode_withdraw = $request->kode_withdraw;
        $data->bank_name = $request->bank_name;
        $data->no_rek = $request->no_rek;
        $data->nama_rek = $request->nama_rek;
        $data->jumlah = $request->jumlah;
        $data->status = 'Proses';
        $data->save();        

        toast('Data Withdraw Berhasil Dibuat','success');
        return redirect()->route('withdraw.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
