<?php

namespace App\Http\Controllers;

use App\Kodereferal;
use App\Mahasiswa;
use App\Profile;
use App\Product;
use App\User;
use Illuminate\Http\Request;

class KodereferalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $profile = Profile::first();
        $kode = Kodereferal::with('mahasiswa')->paginate(5);
        // dd($produk);

        if ($request->cari) {
            if ($request->cari == "nama") {
                $mhs = Mahasiswa::where('nama')->latest()->paginate(5);
            } else {
                $mhs = Mahasiswa::where(function ($mhs) use ($request) {
                    $mhs->where('nama', 'like', "%$request->cari%");
                })->latest()->paginate(5);
            }
        }

        return view('admin.kodereferal.index', compact('profile', 'kode'));
    }

    public function listRef(){
        $user = User::where('name')->get();
        $profile = Profile::first();
        $product = Product::all();
        $cookie = cookie('dw-afiliasi', json_encode($user), 2880);
        // dd($cookie);

        return redirect(route('listkeranjangbelanja.ref'))->cookie($cookie);
    }

    public function listProdRef($user){
        $code = $user;
        $user = User::where('name')->get($user);
        $profile = Profile::first();
        $product = Product::all();
        $cookie = cookie('dw-afiliasi', json_encode($code), 2880);
        // dd($cookie);

        return redirect(route('infoproduk.ref'))->cookie($cookie);
    }

    public function afiliasi(){
        // $code = $user;
        $user = User::where('name')->get();
        $profile = Profile::first();
        $product = Product::all();
        $cookie = cookie('dw-afiliasi', json_encode($user), 2880);
        dd($cookie);

        return redirect(route('welcome.ref'))->cookie($cookie);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mhs = Mahasiswa::all();
        return view('admin.kodereferal.create', compact('mhs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_referal'      => 'required',
        ]);

        $data = $request->all();
        // dd($data);
        Kodereferal::create($data);

        toast('Link Referal Berhasil Ditambahkan','success');
        return redirect('linkreferal');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kodereferal  $kodereferal
     * @return \Illuminate\Http\Response
     */
    public function show(Kodereferal $kodereferal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kodereferal  $kodereferal
     * @return \Illuminate\Http\Response
     */
    public function edit(Kodereferal $kodereferal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kodereferal  $kodereferal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kodereferal $kodereferal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kodereferal  $kodereferal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kodereferal $kodereferal)
    {
        //
    }
}
