<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profil = Profile::all();
        $profile = Profile::first();
        return view('admin.profile.index', compact('profil', 'profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::find($id)->first();
        return view('admin.profile.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'namaprofile'   => 'required',
            'alamat'        => 'required',
            'email'         => 'required',
            'no_tlp'        => 'required',
            'deskripsi'     => 'required',
        ]);

        $data = $request->all();
        $profil = Profile::findorfail($id);
        $data['foto1'] = $profil->foto1;
        $data['foto2'] = $profil->foto2;
        $data['foto3'] = $profil->foto3;

        if($request->has('foto1')){
            $foto = $request->foto1;
            $new_foto = time() . 'profile' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uploads/profile/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto1'] = $new_foto;
        }
        if($request->has('foto2')){
            $foto = $request->foto2;
            $new_foto = time() . 'profile' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uploads/profile/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto2'] = $new_foto;
        }
        if($request->has('foto3')){
            $foto = $request->foto3;
            $new_foto = time() . 'profile' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uploads/profile/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto3'] = $new_foto;
        }

        $profil->update($data);

        toast('Profile Berhasil Diupdate','success');
        return redirect('profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
