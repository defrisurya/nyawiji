<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Withdraw;

class TransaksiwithdrawController extends Controller
{
    public function index(){
        $value = Withdraw::paginate(5);

        return view('admin.transaksiwithdraw.index', compact('value'));
    }

    public function show($id){
        $value = Withdraw::find($id);

        return view('admin.transaksiwithdraw.show', compact('value'));
    }

    public function konfirmasiwithdraw($id){
        $data = Withdraw::find($id);

        $data->update([
            'status' => 'Success',
        ]);

        return redirect()->route('admin.withdraw');
    }
}
