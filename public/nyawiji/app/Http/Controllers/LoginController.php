<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        return view('admin.login');
    }

    public function login(Request $request)
    {
        if (Auth::attempt($request->only('email', 'password'))) {
            return redirect('dashboard');
        }

        return redirect('administrator');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/');
    }
}
