<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keranjangbeli extends Model
{
    protected $table = 'keranjangbelis';
    protected $fillable = ['qty', 'produk_id', 'foto1', 'nama', 'berat', 'harga', 'ukuran', 'warna', 'status'];
}
