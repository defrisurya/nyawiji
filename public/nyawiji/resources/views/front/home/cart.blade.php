<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Keranjang Belanja</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
        integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="{{ asset('front') }}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('front') }}/css/prettyPhoto.css" rel="stylesheet">
    <link href="{{ asset('front') }}/css/price-range.css" rel="stylesheet">
    <link href="{{ asset('front') }}/css/animate.css" rel="stylesheet">
    <link href="{{ asset('front') }}/css/main.css" rel="stylesheet">
    <link href="{{ asset('front') }}/css/responsive.css" rel="stylesheet">
</head>

<body>
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="{{ route('welcome') }}">Home</a></li>
                    <li class="active">Keranjang Belanja</li>
                </ol>
            </div>
            <div>
                <ol class="breadcrumb" style="background: #FE980F">
                    <li><a href="{{ url('/#product') }}" style="color: #FFFFFF; text-decoration: none;"><i
                                class="fas fa-arrow-left">&nbsp;Belanja
                                Lagi</i></a>
                    </li>
                </ol>
            </div>
            <form action="{{ route('proses.orderbeli') }}" method="POST">
                @csrf
                <div class="table-responsive cart_info">
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu">
                                <td class="image">Item Produk</td>
                                <td class="description"></td>
                                <td class="price">QTY</td>
                                <td class="total">Harga</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($carts as $key => $item)
                                <tr>
                                    <td class="cart_product">
                                        <img src="{{ asset('uploads/produk/' . $item['foto1']) }}" alt=""
                                            style="width: 80px; height: 80px; object-fit: cover">
                                    </td>
                                    <td class="cart_description">
                                        <h4>{{ $item['nama'] }}</h4>
                                        <p>Ukuran : {{ $item['ukuran'] }} &nbsp; Warna : {{ $item['warna'] }}</p>
                                    </td>
                                    <td class="cart_description">
                                        <p>
                                            <strong>{{ $item['qty'] }}</strong>
                                        </p>
                                    </td>
                                    <td class="cart_total">
                                        <p class="cart_total_price">Rp.
                                            {{ number_format($item['harga'] * $item['qty']) }}</p>
                                    </td>
                                    <td class="cart_delete" style="margin-right: 10px">
                                        <a class="cart_quantity_delete" href="{{ route('hapus.cart', $item) }}"><i
                                                class="fas fa-times"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6" class="text-center">
                                        <p><i>Keranjang Masih Kosong</i></p>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
        </div>
    </section>
    <!--/#cart_items-->

    <section id="do_action">
        <div class="container">
            <div class="heading">
                <h3>Mohon Lengkapi Form Dibawah Untuk Informasi Pengiriman</h3>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="chose_area">
                        <ul class="user_info">
                            <li>
                                <label>Nama</label>
                                <input type="text" name="namacust">
                            </li>
                            <li>
                                <label>Nomor Telphone</label>
                                <input type="number" name="notlpcust">
                            </li>
                        </ul>
                        <ul>
                            <li class="single_field">
                                <label>Provinsi</label>
                                <select name="province_id" id="province_id">
                                    <option>-- Pilih Provinsi --</option>
                                    @foreach ($provinsi as $row)
                                        <option value="{{ $row['province_id'] }}"
                                            namaprovinsi="{{ $row['province'] }}">{{ $row['province'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </li>
                            <li class="single_field">
                                <label>Kabupaten</label>
                                <select name="kota_id" id="kota_id">
                                    <option>-- Pilih Kabupaten --</option>
                                </select>
                            </li>
                        </ul>
                        <ul>
                            <li class="single_field">
                                <label>Alamat</label>
                                <textarea name="alamatcust" id="" cols="30" rows="6"></textarea>
                            </li>
                        </ul>
                        <ul>
                            <li class="single_field">
                                <label>Ekpedisi</label>
                                <select name="kurir" id="kurir">
                                    <option>-- Pilih Ekspedisi --</option>
                                    <option value="jne">JNE</option>
                                    <option value="tiki">TIKI</option>
                                    <option value="pos">POS INDONESIA</option>
                                </select>
                            </li>
                            <li class="single_field">
                                <label>Layanan</label>
                                <select name="layanan" id="layanan">
                                    <option>-- Pilih Layanan --</option>
                                </select>
                            </li>
                        </ul>
                        {{-- <a class="btn btn-default update" href="">Get Quotes</a>
                        <a class="btn btn-default check_out" href="">Continue</a> --}}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="total_area">
                        <input type="hidden" id="hargatotal" name="totalharga">
                        <input type="hidden" id="ongkoskirim" name="ongkoskirim">
                        <input type="hidden" id="nama_provinsi" name="provinsi">
                        <input type="hidden" id="nama_kota" name="namakota">
                        <input type="hidden" value="{{ $berat }}" name="totalberat">
                        <input type="hidden" value="{{ $jmlqty }}" name="totalqty">
                        <input type="hidden" value="{{ $totalprice }}" name="subtotal">
                        <ul>
                            <li>Sub Total Pembelian <span>Rp {{ number_format($totalprice) }}</span></li>
                            {{-- <li>Eco Tax <span>$2</span></li> --}}
                            <li>Biaya Ongkir <span id="ongkir"></span></li>
                            <li>Total Pembelian <span id="totalharga"></span></li>
                            <button class="btn btn-default check_out">Check Out</button>
                        </ul>
                        {{-- <a class="btn btn-default update" href="">Update</a> --}}
                    </div>
                </div>
            </div>
            </form>
        </div>
    </section>
    <!--/#do_action-->



    <script src="{{ asset('front') }}/js/jquery.js"></script>
    <script src="{{ asset('front') }}/js/bootstrap.min.js"></script>
    <script src="{{ asset('front') }}/js/jquery.scrollUp.min.js"></script>
    <script src="{{ asset('front') }}/js/jquery.prettyPhoto.js"></script>
    <script src="{{ asset('front') }}/js/main.js"></script>

    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            //ini ketika provinsi tujuan di klik maka akan eksekusi perintah yg kita mau
            //name select nama nya "provinve_id" kalian bisa sesuaikan dengan form select kalian
            $('select[name="province_id"]').on('change', function() {
                // membuat variable namaprovinsiku untyk mendapatkan atribut nama provinsi
                var namaprovinsiku = $("#province_id option:selected").attr("namaprovinsi");
                // menampilkan hasil nama provinsi ke input id nama_provinsi
                $("#nama_provinsi").val(namaprovinsiku);

                // kita buat variable provincedid untk menampung data id select province
                let provinceid = $(this).val();
                //kita cek jika id di dpatkan maka apa yg akan kita eksekusi
                if (provinceid) {
                    // jika di temukan id nya kita buat eksekusi ajax GET
                    jQuery.ajax({
                        // url yg di root yang kita buat tadi
                        url: "/kota/" + provinceid,
                        // aksion GET, karena kita mau mengambil data
                        type: 'GET',
                        // type data json
                        dataType: 'json',
                        // jika data berhasil di dapat maka kita mau apain nih
                        success: function(data) {
                            // jika tidak ada select dr provinsi maka select kota kososng / empty
                            $('select[name="kota_id"]').empty();
                            // jika ada kita looping dengan each
                            $.each(data, function(key, value) {
                                // perhtikan dimana kita akan menampilkan data select nya, di sini saya memberi name select kota adalah kota_id
                                $('select[name="kota_id"]').append('<option value="' +
                                    value.city_id + '" namakota="' + value.type +
                                    ' ' + value.city_name + '">' + value.type +
                                    ' ' + value.city_name + '</option>');
                            });
                        }
                    });
                } else {
                    $('select[name="kota_id"]').empty();
                }
            });
            //memberikan action ketika select name kota_id di select
            $('select[name="kota_id"]').on('change', function() {
                // membuat variable namakotaku untyk mendapatkan atribut nama kota
                var namakotaku = $("#kota_id option:selected").attr("namakota");
                // menampilkan hasil nama provinsi ke input id nama_provinsi
                $("#nama_kota").val(namakotaku);
            });
        });
    </script>

    {{-- get ongkir --}}
    <script>
        var berat = <?php echo json_encode($berat); ?>;
        $('select[name="kurir"]').on('change', function() {
            // kita buat variable untuk menampung data data dari  inputan
            // name city_origin di dapat dari input text name city_origin
            // let origin = $("input[name=city_origin]").val();
            // name kota_id di dapat dari select text name kota_id
            let destination = $("select[name=kota_id]").val();
            // name kurir di dapat dari select text name kurir
            let courier = $("select[name=kurir]").val();
            // name weight di dapat dari select text name weight
            // let weight = $("input[name=weight]").val();
            // alert(courier);
            if (courier) {
                jQuery.ajax({
                    url: "/origin=" + 39 + "&destination=" + destination + "&weight=" + berat +
                        "&courier=" + courier,
                    type: 'GET',
                    dataType: 'json',
                    success: function(data) {
                        $('select[name="layanan"]').empty();
                        // ini untuk looping data result nya
                        $.each(data, function(key, value) {
                            // ini looping data layanan misal jne reg, jne oke, jne yes
                            $.each(value.costs, function(key1, value1) {
                                // ini untuk looping cost nya masing masing
                                // silahkan pelajari cara menampilkan data json agar lebi paham
                                $.each(value1.cost, function(key2, value2) {
                                    $('select[name="layanan"]').append(
                                        '<option value="' + value2.value +
                                        '">' + value1.service + '-' + value1
                                        .description + '-' + value2.value +
                                        '</option>');
                                });
                            });
                        });
                    }
                });
            } else {
                $('select[name="layanan"]').empty();
            }
        });
    </script>

    {{-- get ongkos --}}
    <script>
        var totalan = <?php echo json_encode($totalprice); ?>;
        $('select[name="layanan"]').on('change', function() {

            const formatRupiah = (money) => {
                return new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR',
                    minimumFractionDigits: 0
                }).format(money);
            }
            // console.log(formatRupiah(15000));

            // ongkir
            var ongkos = $("#layanan option:selected").attr("value");
            document.getElementById('ongkir').innerHTML = formatRupiah(ongkos);
            $("#ongkoskirim").val(ongkos);

            // totalbayar
            var totalharga = parseInt(ongkos) + parseInt(totalan);
            console.log(totalharga);
            document.getElementById('totalharga').innerHTML = formatRupiah(totalharga);
            $("#hargatotal").val(totalharga);
        });
    </script>
</body>

</html>
