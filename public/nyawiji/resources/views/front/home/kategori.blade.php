@extends('front.layout.app')

@section('title', 'Kategori Produk')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-light bg-light my-2">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('welcome') }}">
                <img src="{{ asset('../assets/img/favicon.ico') }}" alt="" width="35px" height="35px" class="d-inline-block align-text-top">
            </a>
            <ul class="nav nav-tabs d-flex">
                <li class="nav-item">
                    <a class="nav-link @if (Request::segment(1) == '') active @endif" href="{{ route('welcome') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if (Request::segment(1) == 'profil') active @endif" href="{{ route('profil') }}">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if (Request::segment(1) == 'contact') active @endif" href="{{ route('contact') }}">Contact Us</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Kategori Makanan-->
    <div class="container-fluid py-3">
        <div class="row with-3d-shadow">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header text-center pb-0 p-3">
                        <h6 class="mb-0" style="color: black">Kategori {{ $kategori->kategori }}</h6>
                        <hr style="color: black">
                    </div>
                    @php
                        $data = $kategori->produk()->get();
                    @endphp
                    <div class="card-body p-3">
                        <div class="row">
                            @forelse ($data as $item)
                                <div class="col-xl-3 col-md-3 mb-3 mt-3">
                                    <div class="position-relative">
                                        <a href="{{ route('infoproduct', $item->id) }}" class="d-block">
                                            <img src="{{ asset('uploads/'.$item->foto1) }}" alt="img-blur-shadow" height="200px" class="d-block w-100 mb-2">
                                        </a>
                                        <p class="text-sm" style="color: black"><b>{{ $item->nama }}</b></p>
                                        <p class="text-sm" style="color: red"><b>Rp. {{ number_format($item->harga) }}</b></p>
                                    </div>
                                </div>
                                @empty
                                <p class="text-center text-sm" style="color: black"><i>Data tidak tersedia</i></p>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Kategori Makanan -->

    @include('front.layout.component.footer')
@endsection
