@extends('front.home.replika.layouts.app')

@section('title', 'Nyawiji')

@section('content')
    @include('front.home.replika.layouts.component.navbar')

    <!-- Slider -->
    <div id="slides-shop" class="cover-slides">
        <ul class="slides-container">
            <li>
                <img src="{{ asset('uploads/profile/' . $profile->foto1) }}">
            </li>
            <li>
                <img src="{{ asset('uploads/profile/' . $profile->foto2) }}">
            </li>
            <li>
                <img src="{{ asset('uploads/profile/' . $profile->foto3) }}">
            </li>
        </ul>
        <div class="slides-navigation">
            <a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
            <a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
        </div>
    </div>
    <!-- End Slider -->

    <!-- Categories  -->
    {{-- <div class="products-box">
        <div class="title-all text-center">
            <h1>Our Categories</h1>
        </div>
        <div class="instagram-box">
            <div class="main-instagram owl-carousel owl-theme">
                <div class="item">
                    <div class="ins-inner-box">
                        <img src="images/instagram-img-01.jpg" alt="" />
                        <div class="text-cat">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <a href="#">
                                <p>See More&nbsp;<i class="fa fa-arrow-circle-right"></i></p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="ins-inner-box">
                        <img src="images/instagram-img-02.jpg" alt="" />
                        <div class="text-cat">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <a href="#">
                                <p>See More&nbsp;<i class="fa fa-arrow-circle-right"></i></p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="ins-inner-box">
                        <img src="images/instagram-img-03.jpg" alt="" />
                        <div class="text-cat">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <a href="#">
                                <p>See More&nbsp;<i class="fa fa-arrow-circle-right"></i></p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="ins-inner-box">
                        <img src="images/instagram-img-04.jpg" alt="" />
                        <div class="text-cat">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <a href="#">
                                <p>See More&nbsp;<i class="fa fa-arrow-circle-right"></i></p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="ins-inner-box">
                        <img src="images/instagram-img-05.jpg" alt="" />
                        <div class="text-cat">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <a href="#">
                                <p>See More&nbsp;<i class="fa fa-arrow-circle-right"></i></p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="ins-inner-box">
                        <img src="images/instagram-img-06.jpg" alt="" />
                        <div class="text-cat">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <a href="#">
                                <p>See More&nbsp;<i class="fa fa-arrow-circle-right"></i></p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="ins-inner-box">
                        <img src="images/instagram-img-07.jpg" alt="" />
                        <div class="text-cat">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <a href="#">
                                <p>See More&nbsp;<i class="fa fa-arrow-circle-right"></i></p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="ins-inner-box">
                        <img src="images/instagram-img-08.jpg" alt="" />
                        <div class="text-cat">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <a href="#">
                                <p>See More&nbsp;<i class="fa fa-arrow-circle-right"></i></p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="ins-inner-box">
                        <img src="images/instagram-img-09.jpg" alt="" />
                        <div class="text-cat">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <a href="#">
                                <p>See More&nbsp;<i class="fa fa-arrow-circle-right"></i></p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="ins-inner-box">
                        <img src="images/instagram-img-05.jpg" alt="" />
                        <div class="text-cat">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <a href="#">
                                <p>See More&nbsp;<i class="fa fa-arrow-circle-right"></i></p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- End Categories  -->

    <!-- Products  -->
    <div class="products-box" id="product">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Our Products</h1>
                    </div>
                </div>
            </div>
            <div class="row special-list">
                @forelse ($product as $item)
                    <div class="col-lg-3 col-md-6 special-grid">
                        <div class="products-single fix">
                            <div class="box-img-hover">
                                <div class="type-lb"></div>
                                <img src="{{ asset('uploads/produk/' . $item->foto1) }}" class="img-fluid">
                                <a class="cart"
                                    href="{{ url('infoproduct/' . $item->id . '/replika', $user->kode_referal) }}">
                                    <div class="mask-icon">
                                        <p>Info Product</p>
                                    </div>
                                </a>
                            </div>
                            <div class="why-text">
                                <h4>{{ $item->nama }}</h4>
                                <h5>Rp. {{ number_format($item->harga) }}</h5>
                            </div>
                        </div>
                    </div>
                @empty
                    <i>Data tidak tersedia</i>
                @endforelse
            </div>
        </div>
    </div>
    <!-- End Products  -->

    <!-- Back to Top -->
    <a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>
    <!-- End Back to Top -->

    @include('layout.component.footer')
@endsection
