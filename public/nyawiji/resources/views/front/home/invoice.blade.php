<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Invoice</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('front') }}/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('front') }}/dist/css/adminlte.min.css">
</head>

<body class="hold-transition sidebar-mini">

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="callout callout-info">
                        <h5><i class="fas fa-info"></i> Bukti Transaksi Pembelian</h5>
                    </div>

                    <!-- Main content -->
                    <div class="invoice p-3 mb-3">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    <img src="{{ asset('images/nyawiji-modified.png') }}"
                                        class="navbar-brand-img h-100" alt="main_logo"
                                        style="width: 30px; height: 30px"> Nyawiji
                                    <small class="float-right">Tanggal Transaksi :
                                        {{ Carbon\Carbon::parse($data->tgl_transaksi)->isoFormat('D MMMM Y') }}</small>
                                </h4>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                                Dari :
                                <address>
                                    <strong>Nyawiji</strong><br>
                                    {{ $profile->alamat }}<br>
                                    Phone : {{ $profile->no_tlp }}<br>
                                    Email : {{ $profile->email }}
                                </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                Kepada :
                                <address>
                                    <strong>{{ $data->namacust }}</strong><br>
                                    {{ $data->alamatcust }}<br>
                                    Phone : {{ $data->notlpcust }}<br>
                                    Provinsi : {{ $data->provinsi }}<br>
                                    Kabupaten : {{ $data->kabupaten }}
                                    {{-- Email: john.doe@example.com --}}
                                </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                <b>Kode Invoice #{{ $data->kode_invoice }}</b><br>
                                <br>
                                <b>Kode Referal :</b> {{ $data->kode_referal }}<br>
                                <b>Jasa Kirim :</b> {{ $data->kurir }}<br>
                                <b>Status Pembayaran :</b>
                                @if ($data->status_pembayaran == 'Menunggu Pembayaran')
                                    <span style="color: red">{{ $data->status_pembayaran }}</span>
                                @endif
                                @if ($data->status_pembayaran == 'Terbayar')
                                    <span style="color: green">{{ $data->status_pembayaran }}</span>
                                @endif
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <!-- Table row -->
                        <div class="row">
                            <div class="col-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Qty</th>
                                            <th>Produk</th>
                                            <th>Ukuran</th>
                                            <th>Warna</th>
                                            <th>Harga</th>
                                        </tr>
                                    </thead>
                                    @foreach ($data->detailtransaksi as $item)
                                        <tbody>
                                            <tr>
                                                <td>{{ $item['qty'] }}</td>
                                                <td>
                                                    <img src="{{ asset('uploads/produk/' . $item['foto']) }}" alt=""
                                                        style="width: 80px; height: 80px; object-fit: cover">
                                                </td>
                                                <td>{{ $item['ukuran'] }}</td>
                                                <td>{{ $item['warna'] }}</td>
                                                <td>Rp. {{ number_format($item['harga'] * $item['qty']) }}</td>
                                            </tr>
                                        </tbody>
                                    @endforeach
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <!-- accepted payments column -->
                            <div class="col-6">
                                <p class="lead">Informasi Pembayaran :</p>
                                {{-- <img src="{{ asset('front') }}/dist/img/credit/visa.png" alt="Visa">
                                <img src="{{ asset('front') }}/dist/img/credit/mastercard.png" alt="Mastercard">
                                <img src="{{ asset('front') }}/dist/img/credit/american-express.png"
                                    alt="American Express">
                                <img src="{{ asset('front') }}/dist/img/credit/paypal2.png" alt="Paypal"> --}}

                                <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                                    Segera lakukan pembayaran dalam waktu 1x24 jam. Jika pembayaran dalam waktu 1x24 jam
                                    tidak dilakukan, maka transaksi akan dibatalkan. <br>
                                    Setelah melakukan pembayaran, kirim bukti pembayaran melalui WhatsApp ke nomor :
                                    <br>
                                    <b>0822-898-8982</b> <br>
                                    Atau melalui link WhatsApp di bawah ini : <br>
                                    <a href="https://wa.me/628228898982?text=Konfirmasi%20pembayaran%20untuk%20kode%20Invoice%20{{ $data->kode_invoice }}%20"
                                        target="_blank" class="btn btn-success"><i
                                            class="fab fa-whatsapp"></i>&nbsp;WhatsApp</a>
                                </p>
                            </div>
                            <!-- /.col -->
                            <div class="col-6">
                                <p class="lead">Amount Due 2/22/2014</p>

                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th>Subtotal</th>
                                            <td>Rp. {{ number_format($data->subtotal) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Ongkos Kirim</th>
                                            <td>Rp. {{ number_format($data->ongkir) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Total</th>
                                            <td>Rp. {{ number_format($data->total_harga) }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <!-- this row will not appear when printing -->
                        <div class="row no-print mt-5">
                            <div class="col-12">
                                <a href="{{ route('welcome') }}" rel="noopener"
                                    class="btn btn-primary float-start"><i class="fas fa-arrow-left"></i>
                                    Kembali</a>
                                <a href="{{ route('print-invoice', $data->id) }}" rel="noopener" target="_blank"
                                    class="btn btn-warning float-right"><i class="fas fa-print"></i> Print Bukti
                                    Transaksi</a>
                                {{-- <button type="button" class="btn btn-success float-right"><i
                                        class="far fa-credit-card"></i> Submit
                                    Payment
                                </button>
                                <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                                    <i class="fas fa-download"></i> Generate PDF
                                </button> --}}
                            </div>
                        </div>
                    </div>
                    <!-- /.invoice -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('front') }}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('front') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('front') }}/dist/js/adminlte.min.js"></script>
</body>

</html>
