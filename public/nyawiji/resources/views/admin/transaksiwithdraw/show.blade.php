@extends('layout.admin_layouts')

@section('title', 'Administrator | Withdraw')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur"
        navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark"
                            href="{{ route('dashboard') }}">Administrator</a></li>
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark"
                            href="{{ route('withdraw.index') }}">Data Withdraw</a></li>
                </ol>
                <h6 class="font-weight-bolder mb-0">Detail Data Withdraw</h6>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                </div>
                <ul class="navbar-nav  justify-content-end">
                    <li class="nav-item d-flex align-items-center">
                        <i class="fa fa-user"></i>
                        <span class="d-sm-inline text-sm">&nbsp;{{ Auth::user()->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Forms -->
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pb-0 p-3">
                        <div class="container">
                            <div class="col-md-6">
                                <label for="harga" class="form-label">Kode Withdraw</label>
                                <input type="text" class="form-control" id="kode_withdraw" name="kode_withdraw"
                                    placeholder="Kode Withdraw" value="{{ $value->kode_withdraw }}" disabled>
                            </div>
                            <div class="col-md-6">
                                <label for="nama" class="form-label">Nama Bank</label>
                                <input type="text" class="form-control" value="{{ $value->bank_name }}" disabled>
                            </div>
                            <div class="col-md-6">
                                <label for="nama" class="form-label">Nomor Rekening</label>
                                <input type="number" class="form-control" id="no_rek" name="no_rek"
                                    placeholder="Nomor Rekening" value="{{ $value->no_rek }}" disabled>
                            </div>
                            <div class="col-md-6">
                                <label for="nama" class="form-label">Atas Nama</label>
                                <input type="text" class="form-control" id="nama_rek" name="nama_rek"
                                    placeholder="Atas Nama" value="{{ $value->nama_rek }}" disabled>
                            </div>
                            <div class="col-md-6">
                                <label for="nama" class="form-label">Jumlah Withdraw</label>
                                <input type="number" class="form-control" id="jumlah" name="jumlah"
                                    placeholder="Jumlah Withdraw" value="{{ $value->jumlah }}" disabled>
                            </div>
                            <div class="col-12 mt-5">
                                <a href="{{ route('admin.withdraw') }}" type="button"
                                    class="btn btn-secondary float-start"><i class="fas fa-arrow-left"></i>&nbsp;Kembali</a>
                                <a href="{{ route('konfirmasi.withdraw', $value->id) }}"
                                    class="btn btn-success float-end"><i class="fas fa-check"></i>&nbsp;Konfirmasi</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
