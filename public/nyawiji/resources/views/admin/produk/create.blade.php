@extends('layout.admin_layouts')

@section('title', 'Administrator | Tambah Produk')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur"
        navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark"
                            href="{{ route('dashboard') }}">Administrator</a></li>
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark"
                            href="{{ route('admin.produk') }}">Produk</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Tambah Produk</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">Tambah Produk</h6>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                </div>
                <ul class="navbar-nav  justify-content-end">
                    <li class="nav-item d-flex align-items-center">
                        <i class="fa fa-user"></i>
                        <span class="d-sm-inline text-sm">&nbsp;{{ Auth::user()->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Forms -->
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pb-0 p-3">
                        <div class="container">
                            <form class="row g-3" method="POST" action="{{ route('produk.store') }}"
                                enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="berat" value="1">
                                <div class="col-md-6">
                                    <label for="nama" class="form-label">Nama Produk</label>
                                    <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama"
                                        name="nama" placeholder="Nama Produk" value="{{ old('nama') }}">
                                    @error('nama')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="harga" class="form-label">Harga Produk</label>
                                    <input type="number" class="form-control @error('harga') is-invalid @enderror"
                                        id="harga" name="harga" placeholder="Harga Produk" value="{{ old('harga') }}">
                                    @error('harga')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <label for="formFile" class="form-label">Upload Foto</label>
                                    <input class="form-control" required type="file" id="foto1" name="foto1">
                                </div>
                                <div class="col-12">
                                    <label for="formFile" class="form-label">Upload Foto</label>
                                    <input class="form-control" type="file" id="foto2" name="foto2">
                                </div>
                                <div class="col-12">
                                    <label for="formFile" class="form-label">Upload Foto</label>
                                    <input class="form-control" type="file" id="foto3" name="foto3">
                                </div>
                                <div class="col-12">
                                    <label for="formFile" class="form-label">Upload Foto</label>
                                    <input class="form-control" type="file" id="foto4" name="foto4">
                                </div>
                                <div class="col-md-6">
                                    <label for="size" class="form-label">Ukuran Produk</label>
                                    <select name="ukuran[]" multiple="multiple"
                                        class="selectpicker show-tick form-control size">
                                        <option value="All Size">All Size</option>
                                        <option value="S">S</option>
                                        <option value="M">M</option>
                                        <option value="L">L</option>
                                        <option value="XL">XL</option>
                                        <option value="XXL">XXL</option>
                                        <option value="3XL">3XL</option>
                                        <option value="4XL">4XL</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="color" class="form-label">Warna Produk</label>
                                    <select multiple="multiple" name="warna[]"
                                        class="selectpicker show-tick form-control color">
                                        <option value="Hitam">Hitam</option>
                                        <option value="Putih">Putih</option>
                                        <option value="Merah">Merah</option>
                                        <option value="Navy">Navy</option>
                                        <option value="Grey">Grey</option>
                                        <option value="Kuning">Kuning</option>
                                        <option value="Biru">Biru</option>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label for="deskripsi" class="form-label">Deskripsi Produk</label>
                                    <textarea name="deskripsi" id="deskripsi"
                                        class="@error('deskripsi') is-invalid @enderror">{{ old('deskripsi') }}</textarea>
                                    @error('deskripsi')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <a href="{{ route('admin.produk') }}" type="button"
                                        class="btn btn-secondary float-start">Batal</a>
                                    <button type="submit" class="btn btn-primary float-end">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        CKEDITOR.replace('deskripsi');
    </script>
    <script>
        $(document).ready(function() {
            $('.size').select2();
        });
        $(document).ready(function() {
            $('.color').select2();
        });
    </script>
@endsection
