<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="{{ asset('../assets/img/favicon.ico') }}">
    <title>Administrator | Detail Data Transaksi</title>

    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css2?family=Lora&display=swap:400,500,600" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="../css/nucleo-icons.css" rel="stylesheet" />
    <link href="../css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="{{ asset('../css/nucleo-svg.css') }}" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="{{ asset('../css/soft-ui-dashboard.css?v=1.0.3') }}" rel="stylesheet" />
    <!-- Ck Editor -->
    <script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <!-- Select2 -->
    <link href="{{ asset('select2/css/select2.min.css') }}" rel="stylesheet" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('front') }}/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('front') }}/dist/css/adminlte.min.css">
</head>

<body class="g-sidenav-show  bg-gray-100">
    @include('sweetalert::alert')

    @include('layout.component.sidebar')

    <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="callout callout-info">
                            <h5><i class="fas fa-info"></i> Bukti Transaksi Pembelian</h5>
                        </div>

                        <!-- Main content -->
                        <div class="invoice p-3 mb-3">
                            <!-- title row -->
                            <div class="row">
                                <div class="col-12">
                                    <h4>
                                        <img src="{{ asset('images/nyawiji-modified.png') }}"
                                            class="navbar-brand-img h-100" alt="main_logo"
                                            style="width: 30px; height: 30px"> Nyawiji
                                        <small class="float-right">Tanggal Transaksi :
                                            {{ Carbon\Carbon::parse($transaksi->tgl_transaksi)->isoFormat('D MMMM Y') }}</small>
                                    </h4>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- info row -->
                            <br>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    Dari :
                                    <address>
                                        <strong>Nyawiji</strong><br>
                                        {{ $profile->alamat }}<br>
                                        Phone : {{ $profile->no_tlp }}<br>
                                        E-mail : {{ $profile->email }}
                                    </address>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Kepada :
                                    <address>
                                        <strong>{{ $transaksi->namacust }}</strong><br>
                                        {{ $transaksi->alamatcust }}<br>
                                        Phone : {{ $transaksi->notlpcust }}<br>
                                        {{-- Email : john.doe@example.com --}}
                                        Provinsi : {{ $transaksi->provinsi }}<br>
                                        Kabupaten : {{ $transaksi->kabupaten }}<br>
                                    </address>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <b>Kode Invoice #{{ $transaksi->kode_invoice }}</b><br>
                                    <br>
                                    <b>Kode Referal :</b> {{ $transaksi->kode_referal }}<br>
                                    <b>Jasa Kirim :</b> {{ $transaksi->kurir }}<br>
                                    @if ($transaksi->status_pembayaran == 'Menunggu Pembayaran')
                                        <b>Status Pembayaran :</b>
                                        <span
                                            style="color: rgb(255, 102, 0)">{{ $transaksi->status_pembayaran }}</span>
                                    @endif
                                    @if ($transaksi->status_pembayaran == 'Terbayar')
                                        <b>Status Pembayaran :</b>
                                        <span style="color: green">{{ $transaksi->status_pembayaran }}</span>
                                    @endif
                                    @if ($transaksi->status_pembayaran == 'Gagal')
                                        <b>Status Pembayaran :</b>
                                        <span style="color: red">{{ $transaksi->status_pembayaran }}</span>
                                    @endif
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                            <!-- Table row -->
                            <div class="row">
                                <div class="col-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Qty</th>
                                                <th>Nama Produk</th>
                                                <th>Ukuran</th>
                                                <th>Warna</th>
                                                <th>Harga</th>
                                            </tr>
                                        </thead>
                                        @foreach ($data->detailtransaksi as $item)
                                            <tbody>
                                                <tr>
                                                    <td>{{ $item['qty'] }}</td>
                                                    <td>
                                                        <img src="{{ asset('uploads/produk/' . $item['foto']) }}"
                                                            alt="" style="width: 80px; height: 80px; object-fit: cover">
                                                    </td>
                                                    <td>{{ $item['ukuran'] }}</td>
                                                    <td>{{ $item['warna'] }}</td>
                                                    <td>Rp. {{ number_format($item['harga'] * $item['qty']) }}</td>
                                                </tr>
                                            </tbody>
                                        @endforeach
                                    </table>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                            <div class="row">
                                <!-- accepted payments column -->
                                <div class="col-6">
                                    {{-- <p class="lead">Metode Pembayaran :</p>
                                    <img src="{{ asset('front') }}/dist/img/credit/visa.png" alt="Visa">
                                    <img src="{{ asset('front') }}/dist/img/credit/mastercard.png" alt="Mastercard">
                                    <img src="{{ asset('front') }}/dist/img/credit/american-express.png"
                                        alt="American Express">
                                    <img src="{{ asset('front') }}/dist/img/credit/paypal2.png" alt="Paypal">

                                    <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning
                                        heekya handango imeem
                                        plugg
                                        dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                                    </p> --}}
                                </div>
                                <!-- /.col -->
                                <div class="col-6">
                                    {{-- <p class="lead">Amount Due 2/22/2014</p> --}}

                                    <div class="table-responsive">
                                        <table class="table">
                                            {{-- <tr>
                                                <th style="width:50%">Subtotal</th>
                                                <td>$250.30</td>
                                            </tr> --}}
                                            {{-- <tr>
                                                <th>Tax (9.3%)</th>
                                                <td>$10.34</td>
                                            </tr> --}}
                                            <tr>
                                                <th>Subtotal</th>
                                                <td>Rp. {{ number_format($transaksi->subtotal) }}</td>
                                            </tr>
                                            <tr>
                                                <th>Ongkos kirim</th>
                                                <td>Rp. {{ number_format($transaksi->ongkir) }}</td>
                                            </tr>
                                            <tr>
                                                <th>Total</th>
                                                <td>Rp. {{ number_format($transaksi->total_harga) }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                            <!-- this row will not appear when printing -->
                            <div class="row no-print mt-5">
                                <div class="col-12">
                                    <a href="{{ route('admin.transaksibeli') }}" rel="noopener"
                                        class="btn btn-primary float-start"><i class="fas fa-arrow-left"></i>
                                        Kembali</a>
                                    <a href="{{ route('konfirmasi.bayar', $data->id) }}" rel="noopener"
                                        class="btn btn-success float-end"><i class="fas fa-check"></i> Verifikasi
                                        Pembayaran</a>
                                    <a href="{{ route('batal.beli', $data->id) }}" rel="noopener"
                                        class="btn btn-danger float-end"><i class="fas fa-times"></i> Batalkan
                                        Transaksi</a>
                                    {{-- <button type="button" class="btn btn-success float-right"><i
                                            class="far fa-credit-card"></i> Submit
                                        Payment
                                    </button>
                                    <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                                        <i class="fas fa-download"></i> Generate PDF
                                    </button> --}}
                                </div>
                            </div>
                        </div>
                        <!-- /.invoice -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
    </main>
    <!--   Core JS Files   -->
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
    <script src="{{ asset('../js/core/popper.min.js') }}"></script>
    <script src="{{ asset('../js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('../js/plugins/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('../js/plugins/smooth-scrollbar.min.js') }}"></script>
    <script src="{{ asset('../js/soft-ui-dashboard.min.js?v=1.0.3') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('select2/js/select2.min.js') }}"></script>
    <!-- jQuery -->
    <script src="{{ asset('front') }}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('front') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('front') }}/dist/js/adminlte.min.js"></script>

    @yield('script')
</body>

</html>
