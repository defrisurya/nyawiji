@extends('layout.admin_layouts')

@section('title', 'Administrator | Withdraw')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur"
        navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark"
                            href="{{ route('dashboard') }}">Administrator</a></li>
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark"
                            href="{{ route('withdraw.index') }}">Withdraw</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Withdraw</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">Masukkan Data Untuk Melakukan Withdraw</h6>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                </div>
                <ul class="navbar-nav  justify-content-end">
                    <li class="nav-item d-flex align-items-center">
                        <i class="fa fa-user"></i>
                        <span class="d-sm-inline text-sm">&nbsp;{{ Auth::user()->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Forms -->
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pb-0 p-3">
                        <div class="container">
                            <form class="row g-3" method="POST" action="{{ route('withdraw.store') }}">
                                {{ csrf_field() }}
                                <div class="col-md-6">
                                    <label for="harga" class="form-label">Kode Withdraw</label>
                                    <input type="text" class="form-control" id="kode_withdraw" name="kode_withdraw"
                                        placeholder="Kode Withdraw" value="{{ Str::random(15) }}">
                                </div>
                                <div class="col-md-6">
                                    <label for="nama" class="form-label">Nama Bank</label>
                                    <select class="selectpicker show-tick form-control bank" name="bank_name"
                                        id="bank_name">
                                        <option value="">Pilih Bank</option>
                                        <option value="Bank Mandiri">Bank Mandiri</option>
                                        <option value="Bank BCA">Bank BCA</option>
                                        <option value="Bank BRI">Bank BRI</option>
                                        <option value="Bank BNI">Bank BNI</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="nama" class="form-label">Nomor Rekening</label>
                                    <input type="number" class="form-control @error('no_rek') is-invalid @enderror"
                                        id="no_rek" name="no_rek" placeholder="Nomor Rekening"
                                        value="{{ old('no_rek') }}">
                                    @error('no_rek')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="nama" class="form-label">Atas Nama</label>
                                    <input type="text" class="form-control @error('nama_rek') is-invalid @enderror"
                                        id="nama_rek" name="nama_rek" placeholder="Atas Nama"
                                        value="{{ old('nama_rek') }}">
                                    @error('nama_rek')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="nama" class="form-label">Jumlah Withdraw</label>
                                    <input type="number" class="form-control @error('jumlah') is-invalid @enderror"
                                        id="jumlah" name="jumlah" placeholder="Jumlah Withdraw"
                                        value="{{ old('jumlah') }}">
                                    @error('jumlah')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <a href="{{ route('admin.mahasiswa') }}" type="button"
                                        class="btn btn-secondary float-start">Batal</a>
                                    <button type="submit" class="btn btn-primary float-end">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.bank').select2();
        });
    </script>
@endsection
