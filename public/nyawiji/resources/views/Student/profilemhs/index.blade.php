@extends('layout.admin_layouts')

@section('title', 'Administrator | Profile Mahasiswa')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur"
        navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark"
                            href="{{ route('dashboard') }}">Administrator</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Profile Mahasiswa</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">Profile Mahasiswa</h6>
                <li class="nav-item d-xl-none mt-3 d-flex align-items-center">
                    <a href="#" class="nav-link text-body p-0" id="iconNavbarSidenav">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </a>
                </li>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                    <div class="input-group">
                    </div>
                </div>
                <ul class="navbar-nav  justify-content-end">
                    <li class="nav-item d-flex align-items-center">
                        <i class="fa fa-user"></i>
                        <span class="d-sm-inline text-sm">&nbsp;{{ Auth::user()->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <div class="row mt-4">
        <div class="col-lg-8">
            <div class="card h-100 p-3">
                <div class="overflow-hidden position-relative border-radius-lg bg-cover h-100"
                    style="background-image: url('../images/ivancik.jpg');">
                    <span class="mask bg-gradient-dark"></span>
                    <div class="card-body position-relative z-index-1 d-flex flex-column h-100 p-3">
                        <h5 class="text-white font-weight-bolder mb-4 pt-2">Informasi Profile</h5>
                        <p class="text-white">Nama :&nbsp;{{ Auth::user()->name }}</p>
                        <p class="text-white">E-mail :&nbsp;{{ Auth::user()->email }}</p>
                        <p class="text-white">Link Referal :&nbsp;
                            https://nyawiji.com/replika/{{ Auth::user()->kode_referal }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-10">
        {{-- @include('layout.component.footer') --}}
    </div>
@endsection
