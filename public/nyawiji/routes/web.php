<?php

use App\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* Home Page */

Route::get('/', 'HomeController@index')->name('welcome');
Route::get('about', 'HomeController@about')->name('about');
Route::get('contact', 'HomeController@contact')->name('contact');
Route::get('infoproduct/{id}', 'ProductController@show')->name('infoproduct');
Route::get('kategori/{id}', 'HomeController@kategori')->name('kategori');
Route::get('buktiTransaksi/{id}', 'HomeController@invoice')->name('buktiTransaksi');
Route::get('PrintbuktiTransaksi/{id}', 'HomeController@print_invoice')->name('print-invoice');

/* Referal */
Route::get('/replika/{user}', 'ReplikaController@index')->name('welcome.ref');
Route::get('/infoproduct/{id}/replika/{user}', 'ReplikaController@detailProduk')->name('infoproduct.ref');
Route::post('keranjangbeli/replika/{user}', 'ReplikaController@keranjangBeli')->name('keranjangBeli.ref');
Route::get('listkeranjangbelanja/replika/{user}', 'ReplikaController@keranjangBelanja')->name('listkeranjangbelanja.ref');
Route::post('prosesBeli', 'TransaksiController@prosesbeli')->name('proses.orderbeli');
Route::get('hapuscart/{key}/replika/{user}', 'ReplikaController@hapusproduk')->name('hapus.cart');

/* Cart */
Route::post('keranjangbeli', 'HomeController@keranjangbeli')->name('keranjangbeli');
Route::get('listkeranjangbelanja', 'TransaksiController@listkeranjangbeli')->name('listkeranjangbelanja');
Route::get('hapuscart/{key}', 'TransaksiController@hapuscart')->name('hapus.cart');
Route::post('prosesBeli', 'TransaksiController@prosesbeli')->name('proses.orderbeli');

/* End Cart */

/* Raja Ongkir */
Route::get('/province', 'RajaongkirController@get_province')->name('province');
Route::get('/kota/{id}', 'RajaongkirController@get_city')->name('city');
Route::get('/origin={city_origin}&destination={city_destination}&courier={courier}', 'RajaongkirController@get_ongkir')->name('ongkir');
/* End Raja Ongkir */

/* Link Whatsapp */
Route::post('whatsapp', 'LinkController@index')->name('whatsapp');
/* End Home Page */

/* Login Page & Authentikasi */
Route::get('administrator', 'LoginController@index')->name('login');
Route::post('login', 'LoginController@login')->name('postlogin');
Route::get('logout', 'LoginController@logout')->name('logout');
/* End Login Page & Authentikasi */

Route::get('dashboard', 'HomeController@admin')->name('dashboard');

/* Administrator */
Route::group(['middleware' => 'auth', 'CekRole:admin'], function () {
    /* Profile */
    Route::get('profile', 'ProfileController@index')->name('admin.profile');
    Route::get('profile/edit/{id}', 'ProfileController@edit')->name('profile.edit');
    Route::post('profile/update/{id}', 'ProfileController@update')->name('profile.update');

    /* Product */
    Route::get('product', 'ProductController@index')->name('admin.produk');
    Route::get('product/add', 'ProductController@create')->name('produk.add');
    Route::get('product/edit/{id}', 'ProductController@edit')->name('produk.edit');
    Route::post('product', 'ProductController@store')->name('produk.store');
    Route::post('product/update/{id}', 'ProductController@update')->name('produk.update');
    Route::get('product/{id}', 'ProductController@destroy')->name('produk.delete');

    /* Mahasiswa */
    Route::get('mahasiswa', 'MahasiswaController@index')->name('admin.mahasiswa');
    Route::get('mahasiswa/add', 'MahasiswaController@create')->name('mahasiswa.add');
    Route::get('mahasiswa/edit/{id}', 'MahasiswaController@edit')->name('mahasiswa.edit');
    Route::post('mahasiswa', 'MahasiswaController@store')->name('mahasiswa.store');
    Route::post('mahasiswa/update/{id}', 'MahasiswaController@update')->name('mahasiswa.update');
    Route::get('mahasiswa/{id}', 'MahasiswaController@destroy')->name('mahasiswa.delete');

    /* Transaksi */
    Route::get('transaksibeli', 'TransaksibeliController@index')->name('admin.transaksibeli');
    Route::get('transaksibeli/show/{id}', 'TransaksibeliController@show')->name('transaksibeli.show');
    Route::get('konfirmasibayar/{id}', 'TransaksibeliController@konfirmasiBeli')->name('konfirmasi.bayar');
    Route::get('batalBeli/{id}', 'TransaksibeliController@batalBeli')->name('batal.beli');

    /* Withdraw */
    Route::get('transaksiwithdraw', 'TransaksiwithdrawController@index')->name('admin.withdraw');
    Route::get('detailwithdraw/{id}', 'TransaksiwithdrawController@show')->name('detailwithdraw.show');
    Route::get('konfirmasiwithdraw/{id}', 'TransaksiwithdrawController@konfirmasiWithdraw')->name('konfirmasi.withdraw');

    /* Link Referal */
    // Route::get('linkreferal', 'KodereferalController@index')->name('admin.linkreferal');
    // Route::get('linkreferal/add', 'KodereferalController@create')->name('linkreferal.add');
    // Route::get('linkreferal/edit/{id}', 'KodereferalController@edit')->name('linkreferal.edit');
    // Route::post('linkreferal', 'KodereferalController@store')->name('linkreferal.store');
    // Route::post('linkreferal/update/{id}', 'KodereferalController@update')->name('linkreferal.update');
    // Route::get('linkreferal/{id}', 'KodereferalController@destroy')->name('linkreferal.delete');

    /* Kategori */
    // Route::get('kategori', 'KategoriController@index')->name('admin.kategori');
    // Route::get('kategori/edit/{id}', 'KategoriController@edit')->name('kategori.edit');
    // Route::post('kategori', 'KategoriController@store')->name('kategori.store');
    // Route::post('kategori/update/{id}', 'KategoriController@update')->name('kategori.update');
    // Route::get('kategori/delete/{id}', 'KategoriController@destroy')->name('kategori.delete');

});
/* End Administrator */

/* Mahasiswa */
    Route::group(['middleware' => ['auth', 'CekRole:mahasiswa'], 'prefix' => 'Student'], function () {
        Route::namespace('Student')->group(function () {
            Route::resource('profilemhs', 'ProfilemhsController');
            Route::resource('transaksimhs', 'TransaksimhsController');
            Route::resource('withdraw', 'WithdrawController');
        });
    });
/* End Mahasiswa */
