<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeranjangbelisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keranjangbelis', function (Blueprint $table) {
            $table->id();
            $table->string('qty');
            $table->foreignId('produk_id')->constrained('products');
            $table->string('foto1');
            $table->string('nama');
            $table->string('berat');
            $table->string('harga');
            $table->string('ukuran');
            $table->string('warna');
            $table->enum('status', ['Keranjang', 'Checkout']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keranjangbelis');
    }
}
