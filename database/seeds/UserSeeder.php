<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name'          => 'admin',
                'email'         => 'admin@mail.com',
                'role'          => 'admin',
                'no_tlp'        => null,
                'kode_referal'  => null,
                'password'      => bcrypt('password')
            ],
            [
                'name'          => 'mahasiswa',
                'email'         => 'mhs@mail.com',
                'role'          => 'mahasiswa',
                'no_tlp'        => '12345678910',
                'kode_referal'  => Str::random(15),
                'password'      => bcrypt('password')
            ]
        ]);
    }
}
